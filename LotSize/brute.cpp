#include <iostream>
#include <climits>
#include <cmath>
#include <cstring>

using namespace std;

int demanda[100000], custo_armazenagem[100000], custo_producao[100000];

/**
* S = Custo de produção para o periodo (de todos os itens)
* n = Periodo
* h = Altura da árvore
* l = Custo de armazenamento de cada item no periodo
*/

pair<int, int> brute_force(int S, int n, int h, int l)
{
    //cout << "N: " << n << " S: " << S << endl;
    if (n == h-1)
    {
        if (l)
            return make_pair(S, 0);
        else
            return make_pair(S, 1);
    }
    pair<int, int> armazena = brute_force(S+demanda[n+1]*custo_armazenagem[n+1]*(l+1), n+1, h, l+1);
    pair<int, int> produz = brute_force(S + custo_producao[n+1], n+1, h, 0);
    if (armazena.first < produz.first)
        return make_pair(armazena.first, (armazena.second + (1u << (h - (n + 1)))));
    return make_pair(produz.first, produz.second);
}

int main()
{
    int i, periodos;
    cin >> periodos;
    for (i = 0; i < periodos; ++i)
        cin >> custo_producao[i];
    for (i = 0; i < periodos; ++i)
        cin >> demanda[i];
    for (i = 0; i < periodos; ++i)
        cin >> custo_armazenagem[i];
    pair<int, int> par = brute_force(custo_producao[0], 0, periodos, 0);
    cout << par.first << endl;
    
    cout << "Brute Force: ";
    for (int i = periodos-1; i >= 0; --i)
        cout << ((par.second >> i) & 1u);
    cout << endl;
}
