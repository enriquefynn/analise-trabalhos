#include <iostream>
#include <climits>
#include <cmath>
#include <cstring>
#define TAM 10000000
using namespace std;

int demanda[TAM], custo_armazenagem[TAM], custo_producao[TAM], custo_minimo[TAM], produz_em[TAM];
void lot_size_pd(int periodos)
{
	int i, j, h, k;
	// F(0) = 0
	custo_minimo[0] = 0;
	produz_em[0] = 0;
	// F(1) = S1
	custo_minimo[1] = custo_producao[0];
	produz_em[1] = 1;
	for (i = 2; i <= periodos; ++i)
	{
		int minimo = INT_MAX, custo = 0;
		// Adota a politica de custo minimo com base na melhor opção de produção/estocagem
		for (j = 1; j < i; ++j)
		{
			custo = 0;
			for (h = j; h < i; ++h)
				for (k = h+1; k <= i; ++k)
					custo += custo_armazenagem[h-1]*demanda[k-1];
			custo += custo_producao[j-1] + custo_minimo[j-1];
			if (custo < minimo)
			{
				minimo = custo;
				produz_em[i] = j;
			}
		}
		// Produz nesse periodo e adota a politica para os anteriores
		custo = custo_producao[i-1] + custo_minimo[i-1]; 
		if (custo < minimo)
		{
			custo_minimo[i] = custo;
			produz_em[i] = i;
		}
		else
			custo_minimo[i] = minimo;
	}	
}

void sol_recover(int i, int periodos)
{
	int c = 0, j = i - produz_em[i];
	if (i > periodos)
		return;
	while (c <= j)
	{
		cout << produz_em[i] + c;
		if (j != 0 && c != j) cout << ",";
		c++;
	}
	cout << "\t";
	sol_recover(i+1, periodos);
}

int main()
{
    int i, periodos;
    cin >> periodos;
    for (i = 0; i < periodos; ++i)
        cin >> custo_producao[i];
    for (i = 0; i < periodos; ++i)
        cin >> demanda[i];
    for (i = 0; i < periodos; ++i)
        cin >> custo_armazenagem[i];

	lot_size_pd(periodos);
	cout << "Custo Minimo: " << "\t";
	for (int i = 1; i <= periodos; ++i)
		cout << custo_minimo[i] << "\t" ;
	cout << endl << "Produz em: " << "\t";
	sol_recover(1, periodos);
	cout << endl;
}
