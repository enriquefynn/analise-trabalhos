#!/usr/bin/env python2
import random
import matplotlib
matplotlib.use('QT4Agg')
import matplotlib.pyplot as mpl

def getInput(i): 
    f = open('input', 'w')
    MAX = 999
    f.write(str(i))
    f.write('\n')
    for j in range(i):
        f.write(str(random.randint(1, MAX)) + ' ')	
    f.write('\n')
    for j in range(i):
        f.write(str(random.randint(1, MAX)) + ' ')	
    f.write('\n')
    for j in range(i):
        f.write(str(random.randint(1, MAX)) + ' ')	
    f.write('\n')

def line2n(st):
    n = ''
    f = False
    for i in range(len(st)):
        if (st[i] == 'm' or st[i] == 's'):
            pass
        elif (st[i] >= '0' and st[i] <= '9') or st[i] == '.':
            f = True
            n+=st[i]
        else:
            if f == True:
                return float(n)

import subprocess
pointsBruteForce =[]
pointsDP =[]
testSpace = []

print "Tamanho BF: "
inp = int(raw_input())
print "Tamanho PD: "
pdtam = int(raw_input())


for i in range(1, inp):
    testSpace.append(i)
    getInput(i)
    output=subprocess.Popen(["time ./brute < input"], stderr=subprocess.PIPE, shell="True")
    pointsBruteForce.append(line2n(output.communicate()[1]))

    output=subprocess.Popen(["time ./pd < input"], stderr=subprocess.PIPE, shell="True")
    pointsDP.append(line2n(output.communicate()[1]))

for i in range(inp, pdtam):
    testSpace.append(i)
    getInput(i)
    output=subprocess.Popen(["time ./pd < input"], stderr=subprocess.PIPE, shell="True")
    pointsDP.append(line2n(output.communicate()[1]))


for ext in range(pdtam-inp):
    pointsBruteForce.append(2*pointsBruteForce[-1])

print pointsBruteForce
print pointsDP

pBF = mpl.plot(testSpace, pointsBruteForce, '--ro')
pDP = mpl.plot(testSpace, pointsDP, '--b^')
mpl.xlabel('Tamanho da entrada')
mpl.ylabel('Segundos')
#mpl.yscale('log')
mpl.legend((pBF[0], pDP[0]), ('Brute Force', 'PD') )
mpl.show()
