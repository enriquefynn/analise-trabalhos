#include <iostream>
#include <cmath>

using namespace std;

// Return -1 if not found a majority element
int majority_elem(int* v, int i, int f)
{
    int p, l, r, lc = 0, rc = 0;
    int n = f - i + 1;
    if (n == 1)
        return v[i];
    if (n == 2)
        return (v[i] == v[f]) ? v[i] : -1;
	p = ceil((i + f) / 2);
    l = majority_elem(v, i, p);
    r = majority_elem(v, p + 1, f);
    if (l == r) return l;
    if (l != -1)
        for (int e = i; e <= f; ++e)
            if (v[e] == l) lc++;
    if (r != -1)
        for (int e = i; e <= f; ++e)
            if (v[e] == r) rc++;
    if (lc > (floor(n / 2) + 1)) return l;
    if (rc > (floor(n / 2) + 1)) return r;
    return -1;
}

int main()
{
    int n;
    while (cin >> n)
    {
        if (n > 0)
        {
            int* v = new int[n];
            for (int i = 0; i < n; ++i)
                cin >> v[i];
            cout << majority_elem(v, 0, n-1) << endl;
            delete[] v;
        }
    }
}
