#!/usr/bin/env python2
import random
import matplotlib
matplotlib.use('QT4Agg')
import matplotlib.pyplot as mpl

def getInput(i): 
	f = open('input', 'w')
	elem=[]
	maj = random.randint(1, 1000000)
	for j in range(i):
		prob = random.randint(0, 1)
		if prob == 1:
			elem.append(maj)
		else:
			elem.append(random.randint(1, 1000000))
	f.write(str(i))
	f.write(' ')
	for j in elem:
		f.write(str(j) + ' ')	

def line2n(st):
	n = ''
	f = False
	for i in range(len(st)):
                if (st[i] == 'm' or st[i] == 's'):
		    pass
                elif (st[i] >= '0' and st[i] <= '9') or st[i] == '.':
                        f = True
			n+=st[i]
		else:
			if f == True:
				return float(n)


import subprocess
pointsMap = []
pointsUMap =[]
pointsDCM =[]
pointsBruteForce =[]
pointsMoore =[]
testSpace = []

print "Tamanho: "
inp = int(raw_input())

for i in range(1, inp):
	testSpace.append(i)
	getInput(i*2000)
	ls_output=subprocess.Popen(["time ./map < input"], stderr=subprocess.PIPE, shell="True")
	pointsMap.append(line2n(ls_output.communicate()[1]))

	ls_output=subprocess.Popen(["time ./unordered-map < input"], stderr=subprocess.PIPE, shell="True")
	pointsUMap.append(line2n(ls_output.communicate()[1]))

	ls_output=subprocess.Popen(["time ./bruteforce < input"], stderr=subprocess.PIPE, shell="True")
	pointsBruteForce.append(line2n(ls_output.communicate()[1]))
	ls_output=subprocess.Popen(["time ./dcm < input"], stderr=subprocess.PIPE, shell="True")
	pointsDCM.append(line2n(ls_output.communicate()[1]))

	ls_output=subprocess.Popen(["time ./moore < input"], stderr=subprocess.PIPE, shell="True")
	pointsMoore.append(line2n(ls_output.communicate()[1]))

print pointsMap
print pointsUMap
print pointsBruteForce
print pointsDCM
print pointsMoore

pMap = mpl.plot(pointsMap, testSpace, '--ro')
pUMap = mpl.plot(pointsUMap, testSpace, '--b^')
pMoore = mpl.plot(pointsMoore, testSpace, '--gd')
pBruteForce = mpl.plot(pointsBruteForce, testSpace, '--ys')
pDCM = mpl.plot(pointsDCM, testSpace, '--m*')
mpl.ylabel('Tamanho da entrada x 10000')
mpl.xlabel("Segundos")
mpl.xscale('log')
mpl.legend( (pMoore[0], pDCM[0], pMap[0], pUMap[0], pBruteForce[0]), ('Moore', 'Divide & Conquer', 'Map','UnorderedMap', 'Brute Force') )
mpl.show()
