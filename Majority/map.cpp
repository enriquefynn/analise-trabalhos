#include <iostream>
#include <map>

using namespace std;

int main()
{
	map<int, int> um;
	int m, n, a, el;
	while(cin >> n)
	{
		um.clear();
		el = -1;
		m = 0;
		for (int i = 0; i < n; ++i)
		{
			cin >> a;
			++um[a];
		}
		for (auto it: um)
		{
			if (it.second > m)
			{
				el = it.first;
				m = it.second;
			}
		}
		if (m > n/2)
			cout << el << endl;
		else
			cout << -1 << endl;
	}

}
