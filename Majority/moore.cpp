#include <iostream>
#include <map>

using namespace std;

int main()
{
	int n, maj_idx, count, v[1000000];
	while(cin >> n)
	{
		maj_idx = 0;
		count = 0;
		for (int i = 0; i < n; ++i)
		{
			cin >> v[i];
			if (v[maj_idx] == v[i])
				++count;
			else
				--count;
			if (count == 0)
			{
				maj_idx = i;
				count = 1;
			}
		}
		int c = 0;
		for (int i = 0; i < n; ++i)
			if (v[i] == v[maj_idx])
				++c;
		if (c > n/2)
			cout << v[maj_idx] << endl;
		else
			cout << -1 << endl;
	}

}
