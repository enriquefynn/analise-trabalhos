#include <iostream>
#include <map>

using namespace std;

int main()
{
	int vet[1000000];
	int m, n, a, el;
	int maux, cmparator;
	int rep;
	while(cin >> n)
	{
		el = -1;
		rep = 0;
		for (int i = 0; i < n; ++i)
			cin >> vet[i];
		for (int i = 0; i < n; ++i)
		{
			cmparator = vet[i];
			maux = 0;
			for (int j = 0; j < n; ++j)
			{
				if (vet[j] == cmparator)
					++maux;
			}
			if (maux > rep)
			{
				el = i;
				rep = maux;
			}
		}
		if (rep > n/2)
			cout << vet[el] << endl;
		else
			cout << -1 << endl;
	}

}
