#include <iostream>
#include <unordered_set>
#include <deque>
#include <vector>
#define MAX 100000

using namespace std;


int n, n2, n3, elem;
deque<int> combi;
unordered_set<int> univ, s[MAX];
unordered_set<int> minSet;
vector<int> sol;
bool term = false;

void comb(int offset, int k)
{
    if (term)
        return;
    if (k == 0)
    {
        for (int i = 0; i < combi.size(); ++i)
        {
            for (auto elem : s[combi[i]])
                minSet.insert(elem);
        }
        if (minSet.size() == univ.size())
        {
            term = true;
            for (int i = 0; i < combi.size(); ++i)
                sol.push_back(combi[i]);
            return;
        }
        minSet.clear();
        return;
    }

    for (int i = offset; i <= n2 - k; ++i)
    {
        combi.push_back(i);
        comb(i+1, k-1);
        combi.pop_back();
    }
}

int main()
{
    cin >> n;
    for (int i = 0; i < n; ++i)
    {
        cin >> elem;
        univ.insert(elem);
    }
    cin >> n2;
    for (int i = 0; i < n2; ++i)
    {
        cin >> n3;
        for (int j = 0; j < n3; ++j)
        {
            cin >> elem;
            s[i].insert(elem);
        }
    }
    for (int i = 1; i <= n2; ++i)
    {
        comb(0, i);
    }
    /*
    for (int i = 0; i < sol.size(); ++i)
        cout << sol[i] << ' ';
        */

    cout << sol.size() << endl;
}
