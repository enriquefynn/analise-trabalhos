#!/usr/bin/env python2
import random
import matplotlib
matplotlib.use('QT4Agg')
import matplotlib.pyplot as mpl

def getInput(i): 
    f = open('input', 'w')
    MAXN = 100000000
    maxlenu = 1000
    print(i)
    rndusize = random.randrange(100, maxlenu)
    f.write(str(rndusize))
    f.write('\n')
    u = set(random.sample(xrange(1,MAXN), rndusize))
    uctr = set(u)
    for j in u:
        f.write(str(j) + ' ')
    f.write('\n')
    
    f.write(str(i))
    f.write('\n')
    for j in range(i-1):
        rndelem = random.randrange(101)
        s = set(random.sample(u, rndelem))
        f.write(str(rndelem))
        f.write('\n')
        for k in s:
            f.write(str(k) + ' ')
        f.write('\n')
        uctr-= s
    if len(uctr) != 0:
        f.write(str(len(uctr)))
        f.write('\n')
        for j in uctr:
            f.write(str(j) + ' ')
    else:
        rndelem = random.randrange(rndusize) + 1
        s = set(random.sample(u, rndelem))
        f.write(str(rndelem))
        f.write('\n')
        for k in s:
            f.write(str(k) + ' ')
    f.write('\n')
    
def line2n(st):
    n = ''
    f = False
    for i in range(len(st)):
        if (st[i] == 'm' or st[i] == 's'):
            pass
        elif (st[i] >= '0' and st[i] <= '9') or st[i] == '.':
            f = True
            n+=st[i]
        else:
            if f == True:
                return float(n)

import subprocess
pointsBruteForce =[]
pointsDP =[]
testSpace = []

print "Tamanho BF: "
inp = int(raw_input())

for i in range(10, inp):
    testSpace.append(i)
    getInput(i)
    output=subprocess.Popen(["time ./brute < input"], stderr=subprocess.PIPE, shell="True")
    pointsBruteForce.append(line2n(output.communicate()[1]))

    output=subprocess.Popen(["time ./aproximada < input"], stderr=subprocess.PIPE, shell="True")
    pointsDP.append(line2n(output.communicate()[1]))

"""
for i in range(inp, pdtam):
    testSpace.append(i)
    getInput(i)
    output=subprocess.Popen(["time ./pd < input"], stderr=subprocess.PIPE, shell="True")
    pointsDP.append(line2n(output.communicate()[1]))


for ext in range(pdtam-inp):
    pointsBruteForce.append(2*pointsBruteForce[-1])

pointsBruteForce = [0.05, 0.06, 0.12, 0.41, 1.09, 2.28, 3.35, 8.18, 10.88, 24.68]
pointsDP = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
aprox = [1, 10/11, 1, 11/12, 12/13, 1, 1, 14/15, 16/18, 17/19]
testSpace = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
"""
print pointsBruteForce
print pointsDP
pBF = mpl.plot(testSpace, pointsBruteForce, '--ro')
pDP = mpl.plot(testSpace, pointsDP, '--b^')
#pRaz = mpl.plot(testSpace, aprox, '--gx')
mpl.xlabel('Tamanho da entrada')
mpl.ylabel('Segundos')
#mpl.yscale('log')
mpl.legend((pBF[0], pDP[0]), ('Exata', 'Aproximada') )
mpl.show()
