#include <iostream>
#include <algorithm>
#include <set>
#include <deque>
#include <vector>
#define MAX 100000

using namespace std;

bool func(set<int> s1, set<int> s2)
{
    return s1.size() > s2.size();
}

int n, n2, n3, elem;
deque<int> combi;
set<int> univ, s[MAX];
set<int> minSet;
vector<int> sol;
bool term = false;

int main()
{
    cin >> n;
    for (int i = 0; i < n; ++i)
    {
        cin >> elem;
        univ.insert(elem);
    }
    cin >> n2;
    for (int i = 0; i < n2; ++i)
    {
        cin >> n3;
        for (int j = 0; j < n3; ++j)
        {
            cin >> elem;
            s[i].insert(elem);
        }
    }
    sort(s, s+n2, func);
    for (int i = 0; i < n2; ++i)
    {
        sol.push_back(i);
        for (auto elem : s[i])
            minSet.insert(elem);
        if (minSet.size() == n)
            break;
    }
	/*
	for (int i = 0; i < sol.size(); ++i)
        cout << sol[i] << ' ';
	*/
	cout << sol.size() << endl;
}
